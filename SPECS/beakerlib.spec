Name:       beakerlib
Summary:    A shell-level integration testing library
Version:    1.17
Release:    19%{?dist}
License:    GPLv2
Group:      Development/Libraries
BuildArch:  noarch
URL:        https://github.com/%{name}
Autoreq:    0
Requires:   nfs-utils
Requires:   /bin/bash
Requires:   /bin/sh
Recommends: /usr/libexec/platform-python
Recommends: /usr/bin/perl
Requires:   grep
Requires:   sed
Requires:   net-tools
Requires:   coreutils
Requires:   tar
Requires:   gzip
Requires:   util-linux
Requires:   which
Requires:   (wget or curl)
Suggests:   wget
Recommends: python3-lxml
Recommends: xmllint
Obsoletes:  rhtslib beaker-lib
Provides:   rhtslib beaker-lib
Conflicts:  beakerlib-redhat < 1-30

BuildRequires: /usr/bin/pod2man
BuildRequires: perl-generators
BuildRequires: util-linux

Source0:    https://github.com/beakerlib/beakerlib/archive/%{name}-%{version}.tar.gz
Source1:    %{name}-tmpfiles.conf

Patch0: bugzilla-links.patch
Patch1: test-built-time.patch
Patch2: result-file.patch
Patch3: ifs-issue.patch
Patch4: journaling-fixes.patch
Patch5: get-text-journal-size.patch
Patch6: var-TEST.patch
Patch7: reduce-meta.patch
Patch8: enable-nested-phases.patch
Patch9: debug-to-console.patch
Patch10: phase-names-sanitization.patch
Patch11: reboot-in-phase.patch
Patch12: rxvt-terminals-coloring.patch
Patch13: persistent-data-load.patch
Patch14: final-summary-in-rlJournalEnd.patch
Patch15: extended-coloring-capabilities.patch
Patch16: unified-footer.patch
Patch17: rlRun-output.patch
Patch18: python2.patch
Patch19: python3.patch
Patch20: srpm-fetch.patch
Patch21: journalling-import-check.patch
Patch22: handle-missing-python.patch
Patch23: wget2curl-fallback.patch
Patch24: platform-python.patch
Patch25: meta-format-fix.patch

%prep
%autosetup -p1

%build
make build

%install
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}
%{!?_tmpfilesdir: %global _tmpfilesdir %{_prefix}/lib/tmpfiles.d/}
rm -rf $RPM_BUILD_ROOT
make PKGDOCDIR=%{_pkgdocdir} DESTDIR=$RPM_BUILD_ROOT install
mkdir -p $RPM_BUILD_ROOT/%{_tmpfilesdir}
install -m 0644 %{SOURCE1} $RPM_BUILD_ROOT/%{_tmpfilesdir}/%{name}.conf

%description
The BeakerLib project means to provide a library of various helpers, which
could be used when writing operating system level integration tests.

%files
%defattr(-,root,root,-)
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/xslt-templates
%dir %{_pkgdocdir}
%dir %{_pkgdocdir}/examples
%dir %{_pkgdocdir}/examples/*
%{_datadir}/%{name}/dictionary.vim
%{_datadir}/%{name}/*.sh
%{_datadir}/%{name}/xslt-templates/*
%{_bindir}/%{name}-*
%{_mandir}/man1/%{name}*1*
%doc %{_pkgdocdir}/*
%config %{_tmpfilesdir}/%{name}.conf

%package vim-syntax
Summary: Files for syntax highlighting BeakerLib tests in VIM editor
Group: Development/Libraries
Requires: vim-common
BuildRequires: vim-common

%description vim-syntax
Files for syntax highlighting BeakerLib tests in VIM editor

%files vim-syntax
%{_datadir}/vim/vimfiles/after/ftdetect/beakerlib.vim
%{_datadir}/vim/vimfiles/after/syntax/beakerlib.vim

%changelog
* Thu Sep 7 2018 Dalibor Pospisil <dapospis@redhat.com> - 1.17-19
- fixed meta file generation
- follow url redirection when using curl
- fixed checking for python interpreter

* Wed Aug 22 2018 Dalibor Pospisil <dapospis@redhat.com> - 1.17-16
- weak dependency on platform-python
- handling of missing python
- fixed srpm fetching
- fallback to curl if wget is not available
- changed requirements structure

* Mon Jun 25 2018 Dalibor Pospisil <dapospis@redhat.com> - 1.17-15
- migrated to python3
- weak dependency of python3-lxml - without this the journal.xml just will not be generated

* Tue May 15 2018 Dalibor Pospisil <dapospis@redhat.com> - 1.17-14
- use python2 as an interpreter of python scripts

* Sat Feb 24 2018 Dalibor Pospisil <dapospis@redhat.com> - 1.17-13
- rlRun -s now waits for output logs to be flushed, bz1361246 + bz1416796

* Wed Feb 14 2018 Iryna Shcherbina <ishcherb@redhat.com> - 1.17-12
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Fri Feb 09 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 1.17-11
- Escape macros in %%changelog

* Sat Feb 3 2018 Dalibor Pospisil <dapospis@redhat.com> - 1.17-9
- support rxvt terminal colors
- fixed persistent data load for bash version <= 4.1.2
- moved printing of final summray to rlJournalEnd
- extended coloring capabilities
- unified footer format

* Fri Jan 26 2018 Dalibor Pospisil <dapospis@redhat.com> - 1.17-7
- phase name sanitization (remove all weird characters)
- allow debug message to to only to console (speeds execution up in debug)
- allow to reboot inside of phase and continue there
- fixed persistent data loading

* Mon Dec 18 2017 Dalibor Pospisil <dapospis@redhat.com> - 1.17-6
- added missing dependecy

* Wed Dec 13 2017 Dalibor Pospisil <dapospis@redhat.com> - 1.17-5
- result file tweaks
- fixed ifs issue
- improved performance of journaling.py
- fixed computing the length of text text journal per phase
- use internal test name and do not touch TEST variable if empty
- omit human readable meta file comments in non-debug mode
- enable nested phases by default


* Fri Oct 20 2017 Dalibor Pospisil <dapospis@redhat.com> - 1.17-4
- updated dependecies set

* Wed Oct 18 2017 Dalibor Pospisil <dapospis@redhat.com> - 1.17-2
- completely reworked getting rpms
- bstor.py rewritten in pure bash
- some doc fixes
- completely rewritten journal
- extended test suite
- support for XSL transformation of journal.xml
- provided xunit.xsl
- libraries are now searched also in /usr/share/beakerlib-libraries

* Wed May 17 2017 Dalibor Pospisil <dapospis@redhat.com> - 1.16-3
- reworked rpm download function and fallbacks, bz1448510
- added links to bugzilla

* Fri Apr 21 2017 Dalibor Pospisil <dapospis@redhat.com> - 1.16-2
- added missing dependency
- updated links to beakerlib's new home, bz1436810
- added rlAssertLesser and rlAssertLesserOrEqual, bz1423488
- added rpm-handling functions rlFetchSrcForInstalled, rlRpmDownload, and rlRpmInstall

* Thu Jan 26 2017 Dalibor Pospisil <dapospis@redhat.com> - 1.15-1
- added rlIsCentOS similar to rlIsRHEL, bz1214190
- added missing dependencies, bz1391969
- make rlRun use internal variables with more unique name, bz1285804
- fix rlRun exitcodes while using various switches, bz1303900
- rlFileRestore now better distinquish betwwen various errorneous situations, bz1370453
- rlService* won't be blocked be less(1) while systemctl redirection is in place, bz1383303
- variable <libPrefix>LibraryDir variable is created for all imported libraries, holding the path to the library source, bz1074487
- all logging messages are now printed to stderr, bz1171881
- wildcard %%doc inclusion in spec, bz1206173
- prevent unbound variables, bz1228264
- new functions rlServiceEnabled/rlServiceDisable for enabling/disabling services, bz1234804
- updated documentation for rlImport -all, bz1246061
- rlAssertNotEquals now accept empty argument, bz1303618
- rlRun now uses better filename for output log, bz1314700
- fixed cosmetic discrepancy in log output, bz1374256
- added documentation reference for bkrdoc, bz843823
- added documentation of the testwatcher feature, bz1218169
- rlServiceRestore can restore all saved services in no parameter provided, bz494318
- rlCheckMount take mount options (ro/rw) into consideration, bz1191627
- added documentation for LOG_LEVEL variable, bz581816

* Thu Oct 29 2015 Dalibor Pospisil <dapospis@redhat.com> - 1.11-1
- fixed bugs  971347, 1076471, 1262888, 1216177, 1184414, 1192535, 1224345,
  1211269, 1224362, 1205330, 1175513, 1211617, 1221352

* Wed Feb 4 2015 Dalibor Pospisil <dapospis@redhat.com> - 1.10-2
- remount if mounting already mounted mount point with options,
  fixes bug 1173623

* Mon Dec 1 2014 Dalibor Pospisil <dapospis@redhat.com> - 1.10-1
- dropped support for rlSEBoolean functions
- fixed bugs 554280, 1003433, 1103137, 1105299, 1124440, 1124454, 1131934,
  1131963, 1136206, 1155158, 1155234, 1158464, 1159191, and 1165265

* Thu Jul 17 2014 Dalibor Pospisil <dapospis@redhat.com> - 1.9-3
- reverted conditional phases support

* Wed Jul 2 2014 Dalibor Pospisil <dapospis@redhat.com> - 1.9-2
- bunch of fixes

* Tue Jun 17 2014 Dalibor Pospisil <dapospis@redhat.com> - 1.9-1
- rebase to upstream 1.9

* Mon Jul 15 2013 Petr Muller <muller@redhat.com> - 1.8-2
- Syntax highlighting in VIM (Filip Holec)

* Fri Jun 07 2013 Petr Muller <muller@redhat.com> - 1.8-1
- Robustify journal against non-ascii in release names (Petr Muller)
- Make PURPOSE file optional (Nikolai Kondrashov)
- Fix doc paths to /usr/share (Petr Muller)
- Fix corner cases of library discovery (Petr Muller)
- Robustify /dev/null usage in rlRun (Petr Muller)
- Provide more information in passed tests' messages (Petr Muller)
- rlService* functions provide more debugging information (Filip Holec)
- fix rlBundleLogs (Filip Holec)
- fix rlAssertGrep parameter processing (Miroslav Franc)

* Thu Apr 25 2013 Petr Muller <muller@redhat.com> - 1.7-1
- rebase to latest upstream
- fix padding around message timestamps (Dalibor Pospisil)

* Wed Apr 10 2013 Petr Muller <muller@redhat.com> - 1.6.99.3-1
- third attempt for upstream release
- show more package information in the header (Petr Muller)
- journal unicode robustifications (Petr Muller)
- fix crashes when /etc/redhat-release is not present (Petr Muller)
- fix searching of library paths in rlImport (Dalibor Pospisil)
- rlImport --all (Dalibor Pospisil)
- rlImport support for RhtsRequires: Library(foo/bar) (Dalibor Pospisil)
- Improved bookkeeping on already imported libraries (Dalibor Pospisil)
- rewritten rlImport to run also on old bash (Dalibor Pospisil)
- fix rlImport for libs with weird characters in name (Dalibor Pospisil)
- rlLogDebug: fix return code (Dalibor Pospisil)
- several more small fixes

* Tue Mar 05 2013 Petr Muller <muller@redhat.com> - 1.6.99.2-1
- use only distro Python, not the SCL one
- fix pipefail detection for older RHELs
- rlAssertRpm --all
- rlAssertBinaryOrigin
- journal can be used as a Python module now

* Mon Jan 21 2013 Petr Muller <muller@redhat.com> - 1.6.99.1-1
- installation machinery fix

* Thu Jan 10 2013 Petr Muller <muller@redhat.com> - 1.6.99-1
- testing the rebase to potential 1.7
- journal storage cleanups and fixes (Jiri Jaburek)
- fixes in rlMount functions (Petr Muller)
- code cleanups
- use non-tmpfs-backed storage where needed (Petr Muller)
- rlFileBackup namespace support (Jiri Jaburek)
- rlImport implementation

* Thu Jul 26 2012 Petr Muller <muller@redhat.com> - 1.6-2
- packaging tweaks

* Thu Jul 26 2012 Petr Muller <muller@redhat.com> - 1.6-1
- update to upstream version
- code clean-up
- fix rlFileBackup behavior with symlinks (Karel Srot)
- fix rlGetDistroRelease for RHEL7 Alphas
- fix journal tracebacks related to time operations

* Mon Jun 11 2012 Petr Muller <muller@redhat.com> - 1.5-2
- extended rlIs{RHEL/Fedora} syntax allowing intervals (Jiri Jaburek)

* Tue May 15 2012 Petr Muller <muller@redhat.com> - 1.5-1
- fix bz754180 (Matej Kollar)
- fork lsb_release and remove dep on lsb_redhat

* Thu Mar 08 2012 Petr Muller <muller@redhat.com> - 1.4.2
- fix rlGetDistro* functions for RHEL7 (Petr Muller)
- fix SELinux detection in rlFileBackup/Restore (Petr Muller)

* Fri Mar 02 2012 Petr Muller <muller@redhat.com> - 1.4-1
- merge upstream changes and bump the version
- added rlIsXXXX functions from RH internal (Petr Muller)
- added COBBLER_SERVER function export if available (Marian Ganisin)
- unified bash syntax (Roman Rakus)
- INFO logs are now shown by default (Petr Splichal)
- rlFileBackup of symlinks improvements (Petr Splichal)
- added a dep on redhat-lsb to accomodate rlIsXXXX
- log colorizing (Petr Splichal)
- fix rlFileRestore problems with symlinks (Filip Skola)
- added timezone information to start/end time (Pavel Holica)
- deprecate the ABORT state (Brano Nater)
- fix rlWatchdog (Mirek Franc)
- rlCheckMount improvements (Brano Nater)
- add a summary of phase results to logfile (Ales Zelinka)
- config option for more verbose journal printing (Jan Hutar)
- Testsuite improvements (Jan Hutar)
- add John Lockhart's deja-summarize)

* Tue Feb 21 2012 Petr Muller <pmuller@redhat.com> - 1.3-5
- rebuild for RHEL7

* Fri Oct 01 2010 Petr Muller <pmuller@redhat.com> - 1.3-4
- fixed bug when rlRun with -l param didn't work properly (Jan Hutar)
- fixed selinux context problems in rlFileRestore (Petr Splichal)

* Wed Jun 09 2010 Petr Muller <pmuller@redhat.com> - 1.3-3
- packaging fix (remove the unnecessary tag from release)

* Wed Jun 09 2010 Petr Muller <pmuller@redhat.com> - 1.3-2
- functions for determining current test status (Ales Zelinka, Petr Splichal]
- removal of unnecessary sync in rlRun (Petr Splichal)
- packaging tuned for rhel6

* Wed May 12 2010 Petr Muller <pmuller@redhat.com> - 1.3-1
- packaging fixes: permission fixes, added dep on python2,
- added examples as documentation files

* Thu Apr 29 2010 Petr Muller <pmuller@redhat.com> - 1.2-1
- packaging fixes: docdir change, specfile tweaks
- using consistently install -p everywhere

* Thu Apr 08 2010 Petr Muller <pmuller@redhat.com> - 1.2-0
- disable the testsuite and removed a 3rd party lib from the tree

* Mon Mar 22 2010 Petr Muller <pmuller@redhat.com> - 1.1-0
- packaging fixes

* Fri Feb 12 2010 Petr Muller <pmuller@redhat.com> - 1.0-3
- fixed bad path preventing tests from running

* Fri Feb 12 2010 Petr Muller <pmuller@redhat.com> - 1.0-2
- zillion of specfile tweaks for Fedora inclusion
- staf-rhts files were removed
- added a LICENSE file
- added a better package summary
- directory structure revamped
- improved rLDejaSum

* Wed Jan 27 2010 Petr Muller <pmuller@redhat.com> - 1.0-1
- genesis of the standalone BeakerLib
